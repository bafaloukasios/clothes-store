//
//  ProductCollectionViewCell.swift
//  Clothes Store
//
//  Created by George Bafaloukas on 15/02/2020.
//  Copyright © 2020 George Bafaloukas. All rights reserved.
//

import UIKit

protocol ProductCollectionViewCellDelegate: class {
    func addToWishList(index: Int)
    func addToCart(index: Int)
}

class ProductCollectionViewCell: UICollectionViewCell {
    
    weak var delegate: ProductCollectionViewCellDelegate?
    var index: Int?
    
    @IBOutlet weak var productImage: UIImageView!
    @IBOutlet weak var productCategory: UILabel!
    @IBOutlet weak var productName: UILabel!
    @IBOutlet weak var productStock: UILabel!
    @IBOutlet weak var basePrice: UILabel!
    @IBOutlet weak var discountedPrice: UILabel!
    @IBOutlet weak var wishListButton: UIButton!
    @IBOutlet weak var cartButton: UIButton!
    
    @IBAction func addToWishListAction(_ sender: Any) {
        guard let productIndex = self.index else {
            return
        }
        self.delegate?.addToWishList(index: productIndex)
    }
    
    @IBAction func addToCartAction(_ sender: Any) {
        guard let productIndex = self.index else {
            return
        }
        self.delegate?.addToCart(index: productIndex)
    }
}
