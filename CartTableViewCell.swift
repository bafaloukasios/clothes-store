//
//  CartTableViewCell.swift
//  Clothes Store
//
//  Created by George Bafaloukas on 16/02/2020.
//  Copyright © 2020 George Bafaloukas. All rights reserved.
//

import UIKit

protocol CartTableViewCellDelegate: class {
    func removeItemInRow(_ row: Int)
}

class CartTableViewCell: UITableViewCell {
    
    weak var delegate: CartTableViewCellDelegate?
    var index: Int?
    @IBOutlet weak var productImage: UIImageView!
    @IBOutlet weak var categoryNameLabel: UILabel!
    @IBOutlet weak var productNameLabel: UILabel!
    @IBOutlet weak var quantityLabel: UILabel!
    @IBOutlet weak var singleItemPrice: UILabel!
    @IBOutlet weak var discountedPrice: UILabel!
    
    
    @IBAction func removeItemAction(_ sender: Any) {
        guard let row = self.index else {
            return
        }
        self.delegate?.removeItemInRow(row)
    }
    
}
