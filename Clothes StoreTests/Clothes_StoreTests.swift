//
//  Clothes_StoreTests.swift
//  Clothes StoreTests
//
//  Created by George Bafaloukas on 15/02/2020.
//  Copyright © 2020 George Bafaloukas. All rights reserved.
//

import XCTest
@testable import Clothes_Store

class Clothes_StoreTests: XCTestCase {

    var productManager: ProductsManager?
    let mockNetworkManager = MockNetworkManager()
    
    override func setUp() {
        self.productManager = ProductsManager()
        self.productManager?.setNetworkManager(self.mockNetworkManager)
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        self.productManager?.reset()
        self.productManager = nil
    }

    func testAddToWishList() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
        guard let localProductsManager = self.productManager else {
            XCTFail("Products manager not initialised")
            return
        }
        let expAdd = expectation(description: "Check Adding products from Wishlist is successful")
        
        var result = false
        
        let testProduct1 = Product(id: 1, name: "Test Product", category: "Mens footware", price: "250", oldPrice: nil, stock: 10)
        
        //Expecting a FALSE as the product is not in the WishList already
        XCTAssertFalse(localProductsManager.isProductInWishlist(testProduct1))
        
        self.productManager?.addProductToWishList(testProduct1, completionHandler: { success in
            //Expecting a success result as the product is not in the WishList already
            result = success
            expAdd.fulfill()
        })
        
        waitForExpectations(timeout: 10) { error in
            if let error = error {
                XCTFail("waitForExpectationsWithTimeout errored: \(error)")
            }
            //Expecting a TRUE result as the product is not in the WishList already
            XCTAssertTrue(result)
            XCTAssertTrue(localProductsManager.isProductInWishlist(testProduct1))
        }
    }
    
    func testRemoveFromWishList() {
        guard let localProductsManager = self.productManager else {
            XCTFail("Products manager not initialised")
            return
        }
        let expRemove = expectation(description: "Check Removing products from Wishlist is successful")
        
        var result = false
        
        let testProduct2 = Product(id: 2, name: "Test Product 2", category: "Women footware", price: "199", oldPrice: nil, stock: 10)
        
        localProductsManager.wishListArray.append(testProduct2)
        
        //Expecting a TRUE as the product is in in the WishList already
        XCTAssertTrue(localProductsManager.isProductInWishlist(testProduct2))
        
        self.productManager?.addProductToWishList(testProduct2, completionHandler: { success in
            //Expecting a success result as the product is not in the WishList already
            result = success
            expRemove.fulfill()
        })
        
        waitForExpectations(timeout: 10) { error in
            if let error = error {
                XCTFail("waitForExpectationsWithTimeout errored: \(error)")
            }
            //Expecting a FALSE result as the product is in the WishList already
            XCTAssertFalse(result)
            XCTAssertFalse(localProductsManager.isProductInWishlist(testProduct2))
        }
    }
    
    func testProductInWishList() {
        guard let localProductsManager = self.productManager else {
            XCTFail("Products manager not initialised")
            return
        }
        let testProduct3 = Product(id: 1, name: "Test Product 3", category: "Mens footware", price: "229", oldPrice: nil, stock: 1)
        let testProduct4 = Product(id: 2, name: "Test Product 4", category: "Women footware", price: "20.0", oldPrice: nil, stock: 10)
        localProductsManager.wishListArray.append(testProduct4)
        
        XCTAssertFalse(localProductsManager.isProductInWishlist(testProduct3))
        XCTAssertTrue(localProductsManager.isProductInWishlist(testProduct4))
    }
    
    func testProductDecoding() {
        let testBundle = Bundle(for: type(of: self))
        if let path = testBundle.path(forResource: "productsResponse", ofType: "json") {
            do {
                let result: Bool
                let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
                if let _ = try? JSONDecoder().decode([Product].self, from: data) {
                    result = true
                } else {
                    result = false
                }
                XCTAssertTrue(result)
              } catch {
                   XCTFail("productsResponse file unable to open")
              }
        } else {
            XCTFail("productsResponse file not found")
        }
    }
    
    func testUpdateProducts() {
        guard let localProductsManager = self.productManager else {
            XCTFail("Products manager not initialised")
            return
        }
        let expUpdate = expectation(description: "Check updating products parsing and return is successful")
        
        self.productManager?.updateProducts({ success in
            expUpdate.fulfill()
        })
        
        waitForExpectations(timeout: 10) { error in
            if let error = error {
                XCTFail("waitForExpectationsWithTimeout errored: \(error)")
            }
            XCTAssertTrue(localProductsManager.productsArray.count == 3)
        }
    }
    
    func testRetrieveCart() {
        guard let localProductsManager = self.productManager else {
            XCTFail("Products manager not initialised")
            return
        }
        
        let expUpdate = expectation(description: "Check retrieving cart parsing and return is successful")
        var result = false
       
        self.productManager?.updateProducts({ success in
            self.productManager?.updateCartItems({ success in
                result = success
                expUpdate.fulfill()
            })
        })
        
        waitForExpectations(timeout: 10) { error in
            if let error = error {
                XCTFail("waitForExpectationsWithTimeout errored: \(error)")
            }
            XCTAssertTrue(result)
            XCTAssertTrue(localProductsManager.cartArray.count == 3)
        }
    }
}
