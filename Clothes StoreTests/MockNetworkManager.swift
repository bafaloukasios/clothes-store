//
//  MockNetworkManager.swift
//  Clothes StoreTests
//
//  Created by George Bafaloukas on 18/02/2020.
//  Copyright © 2020 George Bafaloukas. All rights reserved.
//

import Foundation

class MockNetworkManager: NetworkManager {
    override func retrieveProducts(_ completionHandler: @escaping GetProductsRequestCompletionHandler) {
        let testBundle = Bundle(for: type(of: self))
        if let path = testBundle.path(forResource: "productsResponse", ofType: "json") {
            do {
                let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
                if let products = try? JSONDecoder().decode([Product].self, from: data) {
                    completionHandler(true, products)
                } else {
                    completionHandler(false, nil)
                }
              } catch {
                    completionHandler(false, nil)
              }
        } else {
            completionHandler(false, nil)
        }
    }
    
    override func retrieveCart(_ completionHandler: @escaping GetCartItemsRequestCompletionHandler) {
        let testBundle = Bundle(for: type(of: self))
        if let path = testBundle.path(forResource: "cartResponse", ofType: "json") {
            do {
                let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
                if let products = try? JSONDecoder().decode([CartItem].self, from: data) {
                    completionHandler(true, products)
                } else {
                    completionHandler(false, nil)
                }
              } catch {
                    completionHandler(false, nil)
              }
        } else {
            completionHandler(false, nil)
        }
    }
}
