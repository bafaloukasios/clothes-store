# Clothes Store Demo Project

Demo application of an imaginary Clothes store. 'Clothes store' is an app to demonstrate basic principles of iOS programming,
like consuming data from webservices, handling of asyncronous network actions and archiving/reloading user data.

## Getting Started

To run the application, clone the project and use XCode 11 to build and Run. The project makes use of Cocoapods, therefore please use the 'Clothes Store.xcworkspace'.
Used Pods are commited so no need to run a 'pod install'. In case of build errors, please move to the home project directory and run the 'pod install' command on the Terminal.

### Prerequisites

Xcode 11
Cocoapods

## Running the tests

The project contains 6 Unit Test cases using the XCTest framework, testing the basic functionality of the 'ProductsManager' class.
To run open the project using XCode and run the tests from the 'Test Navigator' tab

## 3rd Party Libraries used

* Crashlytics by Firebase. Crash and performance reporting library.
* JGProgressHUD. An elegant and simple progress HUD for iOS and tvOS.

## Code Structure

### Model:
 - CartItem: A codable struct used to consume the 'cart' response.
 - Product: A codable class used to consume the 'products' response.
 - CartProduct: A class inheriting from 'Product' adding grouping functionality for products added to the cart.

### Managers:
 - ProductManager: Controller Class responsible for handling the application data structure and transactions. Interacting with the NetworkManager and updating Product list, Wishlist and cart.
 - NetworkManager: Class responsible for the network communication with the API and the consuming of the webservices.
 - UserSessionManager: Singleton class hosting the product manager. In a full fat version it would host other session information and the User object.

### Views & ViewControllers:
  - ProductCollectionViewCell, WishlistTableViewCell, CartTableViewCell: Product UITableView or CollectionView Cells for displaying product entries for different app views.
  - ClothesLayout: Custom UICollectionViewLayout for displaying a 2 column vertical collection view
  - HudViewController: UIViewController subclass with helper methods for displaying appropriate JGProgressHUDs
  - ProductsViewController, WishListViewController, CartViewController: Subclasses of HudViewController for the 3 different sections of the app.
  - Main.storyboard: Storyboard file containing all the Views of the application.

## License

MIT License.
© 2020, George Bafaloukas.
