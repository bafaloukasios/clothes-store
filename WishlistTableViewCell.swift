//
//  WishlistTableViewCell.swift
//  Clothes Store
//
//  Created by George Bafaloukas on 15/02/2020.
//  Copyright © 2020 George Bafaloukas. All rights reserved.
//

import UIKit

protocol WishlistTableViewCellDelegate: class {
    func moveProductToCartForIndex(_ index: Int)
}

class WishlistTableViewCell: UITableViewCell {

    weak var delegate: WishlistTableViewCellDelegate?
    var index: Int?
    
    @IBOutlet weak var productImage: UIImageView!
    @IBOutlet weak var productCategory: UILabel!
    @IBOutlet weak var productName: UILabel!
    @IBOutlet weak var productStock: UILabel!
    @IBOutlet weak var basePrice: UILabel!
    @IBOutlet weak var discountedPrice: UILabel!
    @IBOutlet weak var addToCartButton: UIButton!
    @IBOutlet weak var itemInCartView: UIView!
    
    @IBAction func moveToCartAction(_ sender: Any) {
        guard let productIndex = self.index else {
            return
        }
        self.delegate?.moveProductToCartForIndex(productIndex)
    }
    
}
