//
//  ViewController.swift
//  Clothes Store
//
//  Created by George Bafaloukas on 15/02/2020.
//  Copyright © 2020 George Bafaloukas. All rights reserved.
//

import UIKit

class ProductsViewController: HudViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, ProductCollectionViewCellDelegate, ClothesLayoutDelegate {
    
    @IBOutlet weak var productsCollectionView: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let layout = productsCollectionView?.collectionViewLayout as? ClothesLayout {
          layout.delegate = self
        }
        self.showLoadingHud()
        UserSessionManager.sharedInstance.productsManager.updateProducts { [unowned self] success in
            UserSessionManager.sharedInstance.productsManager.updateCartItems()
            DispatchQueue.main.async {
                self.productsCollectionView.reloadData()
                self.hud.dismiss(animated: true)
            }
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.productsCollectionView.reloadData()
    }
    
    //MARK: UICollectionViewDelegate, UICollectionViewDataSource
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return UserSessionManager.sharedInstance.productsManager.productsArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ProductCollectionViewCell", for: indexPath) as? ProductCollectionViewCell else {
            fatalError()
        }
        
        cell.delegate = self
        cell.index = indexPath.row
        
        let product = UserSessionManager.sharedInstance.productsManager.productsArray[indexPath.row]
        
        if product.stock > 0 {
            cell.productStock.text = "\(product.stock) Remaining"
            cell.cartButton.isSelected = true
        } else {
            cell.productStock.text = "Out of stock"
            cell.cartButton.isSelected = false
        }
        
        if let productImage = UIImage(named: String(product.id)) {
            cell.productImage.image = productImage
        } else {
            cell.productImage.image = placeHolderImage
        }
        
        cell.wishListButton.isSelected = UserSessionManager.sharedInstance.productsManager.isProductInWishlist(product)
        cell.productCategory.text = product.category
        cell.productName.text = product.name
        if let oldPrice = product.oldPrice {
            cell.basePrice.text = "£\(oldPrice)"
            cell.discountedPrice.text = "£\(product.price)"
        } else {
            cell.basePrice.text = "£\(product.price)"
            cell.discountedPrice.text = nil
        }
        
        return cell
    }
    
    // MARK: ClothesLayoutDelegate
    

    func collectionView(_ collectionView: UICollectionView, heightForPhotoAtIndexPath indexPath:IndexPath) -> CGFloat {
        return 315
    }
    
    // MARK: ProductCollectionViewCellDelegate
    
    func addToWishList(index: Int) {
        let product = UserSessionManager.sharedInstance.productsManager.productsArray[index]
        UserSessionManager.sharedInstance.productsManager.addProductToWishList(product) { [unowned self] success in
            if success {
                self.showSuccessHudWithMessage("Added to wishlist")
            } else {
                self.showErrorHudWithMessage("Removed from wishlist")
            }
            self.productsCollectionView.reloadData()
        }
    }
    
    func addToCart(index: Int) {
        self.showLoadingHud()
        let product = UserSessionManager.sharedInstance.productsManager.productsArray[index]
        if product.stock > 0 {
            UserSessionManager.sharedInstance.productsManager.addProductToCart(product) { [unowned self] result in
                DispatchQueue.main.async {
                    switch result {
                    case .itemAdded:
                        self.showSuccessHudWithMessage(result.rawValue)
                    case .stockNotSufficient, .productNotFound, .networkError:
                        self.showErrorHudWithMessage(result.rawValue)
                    }
                    self.productsCollectionView.reloadData()
                }
            }
        } else {
            DispatchQueue.main.async {
                self.showErrorHudWithMessage("Stock not sufficient")
            }
        }
    }
}
