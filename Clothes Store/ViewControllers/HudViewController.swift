//
//  HudViewController.swift
//  Clothes Store
//
//  Created by George Bafaloukas on 17/02/2020.
//  Copyright © 2020 George Bafaloukas. All rights reserved.
//

import UIKit
import JGProgressHUD

class HudViewController: UIViewController {

    let hud = JGProgressHUD(style: .dark)
    var placeHolderImage = UIImage(named: "NoImage")
    
    func showLoadingHud() {
        DispatchQueue.main.async {
            self.hud.indicatorView = JGProgressHUDIndeterminateIndicatorView()
            self.hud.textLabel.text = nil
            self.hud.show(in: self.view)
        }
    }
    
    func showSuccessHudWithMessage(_ message: String) {
        self.hud.textLabel.text = message
        self.hud.indicatorView = JGProgressHUDSuccessIndicatorView()
        self.hud.show(in: self.view)
        self.hud.dismiss(afterDelay: 1.0)
    }
    
    func showErrorHudWithMessage(_ message: String) {
        self.hud.textLabel.text = message
        self.hud.indicatorView = JGProgressHUDErrorIndicatorView()
        self.hud.show(in: self.view)
        self.hud.dismiss(afterDelay: 1.0)
    }

}
