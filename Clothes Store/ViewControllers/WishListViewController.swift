//
//  WishListViewController.swift
//  Clothes Store
//
//  Created by George Bafaloukas on 15/02/2020.
//  Copyright © 2020 George Bafaloukas. All rights reserved.
//

import UIKit

class WishListViewController: HudViewController, UITableViewDelegate, UITableViewDataSource, WishlistTableViewCellDelegate {
    
    @IBOutlet weak var productTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        UserSessionManager.sharedInstance.productsManager.wishListUpdateHandler = { [unowned self] (success) in
            if success == true {
                DispatchQueue.main.async {
                    self.productTableView.reloadData()
                }
            }
        }
    }
    
    // MARK: - UITableViewDelegate, UITableViewDataSource
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return UserSessionManager.sharedInstance.productsManager.wishListArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "WishlistTableViewCell", for: indexPath) as? WishlistTableViewCell else {
            fatalError()
        }

        let product = UserSessionManager.sharedInstance.productsManager.wishListArray[indexPath.row]
        let productInCart = UserSessionManager.sharedInstance.productsManager.isProductInCart(product)
        cell.productName.text = product.name
        cell.productCategory.text = product.category
        if let oldPrice = product.oldPrice {
            cell.basePrice.text = "£\(oldPrice)"
            cell.discountedPrice.text = "£\(product.price)"
        } else {
            cell.basePrice.text = "£\(product.price)"
            cell.discountedPrice.text = nil
        }
        
        if product.stock > 0 {
            cell.productStock.text = "\(product.stock) remaining"
            cell.addToCartButton.isHidden = false
        } else {
            cell.productStock.text = "Out of stock"
            cell.addToCartButton.isHidden = true
        }
        
        if let productImage = UIImage(named: String(product.id)) {
            cell.productImage.image = productImage
        } else {
            cell.productImage.image = placeHolderImage
        }
        
        cell.itemInCartView.isHidden = !productInCart
        cell.addToCartButton.isHidden = productInCart
        cell.delegate = self
        cell.index = indexPath.row
        return cell
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
           UserSessionManager.sharedInstance.productsManager.wishListArray.remove(at: indexPath.row)
        }
    }
    
    //MARK: WishlistTableViewCellDelegate
    func moveProductToCartForIndex(_ index: Int) {
        self.showLoadingHud()
        let product = UserSessionManager.sharedInstance.productsManager.wishListArray[index]
        UserSessionManager.sharedInstance.productsManager.moveProductFromWishlistToCart(product) { [unowned self] result in
            DispatchQueue.main.async {
                switch result {
                case .itemAdded:
                    self.showSuccessHudWithMessage(result.rawValue)
                    UserSessionManager.sharedInstance.productsManager.updateCartItems()
                case .stockNotSufficient, .productNotFound, .networkError:
                    self.showErrorHudWithMessage(result.rawValue)
                }
                self.productTableView.reloadData()
            }
        }
    }

}
