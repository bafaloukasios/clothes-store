//
//  CartViewController.swift
//  Clothes Store
//
//  Created by George Bafaloukas on 15/02/2020.
//  Copyright © 2020 George Bafaloukas. All rights reserved.
//

import UIKit

class CartViewController: HudViewController, UITableViewDataSource, UITableViewDelegate, CartTableViewCellDelegate {
    
    @IBOutlet weak var cartTableView: UITableView!
    @IBOutlet weak var itemsInCartLabel: UILabel!
    @IBOutlet weak var savingsLabel: UILabel!
    @IBOutlet weak var orderTotalLabel: UILabel!
    @IBOutlet weak var checkOutButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.showLoadingHud()
        UserSessionManager.sharedInstance.productsManager.cartUpdateHandler = { [unowned self] (success) in
            if success == true {
                DispatchQueue.main.async {
                    self.updateCartValues()
                    self.hud.dismiss()
                }
            }
        }
        UserSessionManager.sharedInstance.productsManager.updateCartItems()
        self.updateCartValues()
    }
    
    // MARK: - UITableViewDataSource, UITableViewDelegate
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return UserSessionManager.sharedInstance.productsManager.cartArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "CartTableViewCell", for: indexPath) as? CartTableViewCell else {
            fatalError()
        }
        
        let cartProduct = UserSessionManager.sharedInstance.productsManager.cartArray[indexPath.row]
        cell.delegate = self
        cell.index = indexPath.row
        cell.categoryNameLabel.text = cartProduct.category
        cell.productNameLabel.text = cartProduct.name
        
        if let oldPrice = cartProduct.oldPrice {
            cell.singleItemPrice.text = "£\(oldPrice)"
            cell.discountedPrice.text = "£\(cartProduct.price)"
        } else {
            cell.singleItemPrice.text = "£\(cartProduct.price)"
            cell.discountedPrice.text = nil
        }
        
        if let productImage = UIImage(named: String(cartProduct.id)) {
            cell.productImage.image = productImage
        } else {
            cell.productImage.image = placeHolderImage
        }
        
        cell.quantityLabel.text = "Items in cart: \(cartProduct.quantity)"
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            self.showLoadingHud()
            let cartProduct = UserSessionManager.sharedInstance.productsManager.cartArray[indexPath.row]
            UserSessionManager.sharedInstance.productsManager.removeCartProduct(cartProduct) { success in
                DispatchQueue.main.async {
                    if success {
                        self.showSuccessHudWithMessage("Product removed")
                    } else {
                        self.showErrorHudWithMessage("Something went wrong")
                    }
                }
            }
        }
    }
    
    //MARK: CartTableViewCellDelegate
    func removeItemInRow(_ row: Int) {
        self.showLoadingHud()
        let cartProduct = UserSessionManager.sharedInstance.productsManager.cartArray[row]
        UserSessionManager.sharedInstance.productsManager.removeSingleItem(cartProduct) { success in
            DispatchQueue.main.async {
                if success {
                    self.showSuccessHudWithMessage("Product removed")
                } else {
                    self.showErrorHudWithMessage("Something went wrong")
                }
            }
        }
    }
    
    //MARK: Private methods
    func updateCartValues() {
        let itemsInCart = UserSessionManager.sharedInstance.productsManager.itemsInCart()
        self.itemsInCartLabel.text = "\(itemsInCart)"
        self.checkOutButton.isEnabled = itemsInCart > 0
        self.orderTotalLabel.text = UserSessionManager.sharedInstance.productsManager.orderTotal()
        self.savingsLabel.text = UserSessionManager.sharedInstance.productsManager.orderSavings()
        self.cartTableView.reloadData()
    }
}
