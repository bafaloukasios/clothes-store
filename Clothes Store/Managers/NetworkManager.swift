//
//  NetworkManager.swift
//  Clothes Store
//
//  Created by George Bafaloukas on 16/02/2020.
//  Copyright © 2020 George Bafaloukas. All rights reserved.
//

import UIKit

public typealias GetProductsRequestCompletionHandler = (_ success: Bool, _ products: [Product]?) -> Void
public typealias GetCartItemsRequestCompletionHandler = (_ success: Bool, _ products: [CartItem]?) -> Void
public typealias AddProductToCartRequestCompletionHandler = (_ response: AddToCartReponseCode?) -> Void
public typealias RemoveProductFromCartRequestCompletionHandler = (_ response: RemoveCartReponseCode?) -> Void

class NetworkManager: NSObject {

    let defaultSession = URLSession(configuration: .default)
    private let baseURL = "https://2klqdzs603.execute-api.eu-west-2.amazonaws.com"
    private let productsEndpoint = "/cloths/products"
    private let cartEndpoint = "/cloths/cart"
    private let API_KEY = "DDE3D1827D-EBB5-41C0-81F4-79506737AD8F"
    
    func retrieveProducts(_ completionHandler: @escaping GetProductsRequestCompletionHandler) {
        var dataTask: URLSessionDataTask?
        
        if let baseURL = URL(string: baseURL), let url = URL(string: productsEndpoint, relativeTo: baseURL) {
            var request = URLRequest(url: url)
            request.setValue("application/json", forHTTPHeaderField: "Accept")
            request.setValue(API_KEY, forHTTPHeaderField: "X-API-KEY")
            dataTask = defaultSession.dataTask(with: request) { data, response, error in
                if let error = error {
                    print("DataTask error: " + error.localizedDescription + "\n")
                    completionHandler(false, nil)
                } else if let data = data, let response = response as? HTTPURLResponse, response.statusCode == 200 {
                    if let products = try? JSONDecoder().decode([Product].self, from: data) {
                        completionHandler(true, products)
                    } else {
                        completionHandler(false, nil)
                    }
                } else {
                    completionHandler(false, nil)
                }
            }
              
            dataTask?.resume()
        }
    }
    
    func retrieveCart(_ completionHandler: @escaping GetCartItemsRequestCompletionHandler) {
        var dataTask: URLSessionDataTask?
        
        if let baseURL = URL(string: baseURL), let url = URL(string: cartEndpoint, relativeTo: baseURL) {
            var request = URLRequest(url: url)
            request.setValue("application/json", forHTTPHeaderField: "Accept")
            request.setValue(API_KEY, forHTTPHeaderField: "X-API-KEY")
            dataTask = defaultSession.dataTask(with: request) { data, response, error in
                if let error = error {
                    print("DataTask error: " + error.localizedDescription + "\n")
                    completionHandler(false, nil)
                } else if let data = data, let response = response as? HTTPURLResponse, response.statusCode == 200 {
                    if let products = try? JSONDecoder().decode([CartItem].self, from: data) {
                        completionHandler(true, products)
                    } else {
                        completionHandler(false, nil)
                    }
                } else {
                    completionHandler(false, nil)
                }
            }
            dataTask?.resume()
        }
    }
    
    func addProductToCart(_ productId: Int, completionHandler: @escaping AddProductToCartRequestCompletionHandler) {
        var dataTask: URLSessionDataTask?
        
        if let baseURL = URL(string: baseURL), let url = URL(string: "\(cartEndpoint)?productId=\(productId)", relativeTo: baseURL) {
            var request = URLRequest(url: url)
            request.setValue("application/json", forHTTPHeaderField: "Accept")
            request.setValue(API_KEY, forHTTPHeaderField: "X-API-KEY")
            request.httpMethod = "POST"
            
            dataTask = defaultSession.dataTask(with: request) { data, response, error in
                if let response = response as? HTTPURLResponse {
                    completionHandler(AddToCartReponseCode(rawValue: response.statusCode))
                } else {
                    completionHandler(nil)
                }
            }
            dataTask?.resume()
        }
    }
    
    func removeProductFromCart(_ cartId: Int, completionHandler: @escaping RemoveProductFromCartRequestCompletionHandler) {
        var dataTask: URLSessionDataTask?
        
        if let baseURL = URL(string: baseURL), let url = URL(string: "\(cartEndpoint)?id=\(cartId)", relativeTo: baseURL) {
            var request = URLRequest(url: url)
            request.setValue("application/json", forHTTPHeaderField: "Accept")
            request.setValue(API_KEY, forHTTPHeaderField: "X-API-KEY")
            request.httpMethod = "DELETE"
            
            dataTask = defaultSession.dataTask(with: request) { data, response, error in
                if let response = response as? HTTPURLResponse {
                    completionHandler(RemoveCartReponseCode(rawValue: response.statusCode))
                } else {
                    completionHandler(nil)
                }
            }
            dataTask?.resume()
        }
    }
    
}
