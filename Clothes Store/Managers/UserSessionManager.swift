//
//  UserSessionManager.swift
//  Clothes Store
//
//  Created by George Bafaloukas on 18/02/2020.
//  Copyright © 2020 George Bafaloukas. All rights reserved.
//

import UIKit

class UserSessionManager: NSObject {
    static let sharedInstance = UserSessionManager()
    
    var productsManager = ProductsManager()
}
