//
//  ProductsManager.swift
//  Clothes Store
//
//  Created by George Bafaloukas on 15/02/2020.
//  Copyright © 2020 George Bafaloukas. All rights reserved.
//

import UIKit

public typealias ProductsRequestCompletionHandler = (_ success: Bool) -> Void
public typealias AddProductsRequestCompletionHandler = (_ addProductResult: AddProductResult) -> Void
public typealias CartUpdateHandler = (_ success: Bool) -> Void


class ProductsManager: NSObject {
    let archiveKey = "UserWishListData"
    
    private var networkManager: NetworkManager?
    
    var cartUpdateHandler: CartUpdateHandler?
    var wishListUpdateHandler: CartUpdateHandler?
    
    var cartArray = [CartProduct]()
    var wishListArray = [Product]() {
        didSet {
            self.wishListUpdateHandler?(true)
        }
    }
    var productsArray = [Product]() {
        didSet {
            self.updateWishListStock()
        }
    }
    
    func setNetworkManager(_ networkManager: NetworkManager) {
        self.networkManager = networkManager
    }
    
    func reset() {
        //Method mostly used for testing
        self.networkManager = nil
        self.productsArray.removeAll()
        self.cartArray.removeAll()
        self.productsArray.removeAll()
    }
    
    func updateProducts(_ completionHandler: @escaping ProductsRequestCompletionHandler) {
        self.networkManager?.retrieveProducts { [unowned self] (success, products) in
            if let productsArray = products {
                self.productsArray = productsArray
            }
            DispatchQueue.main.async {
                completionHandler(success)
            }
        }
    }
    
    func updateCartItems(_ completionHandler: CartUpdateHandler? = nil) {
        self.networkManager?.retrieveCart { [unowned self] (success, cartItems) in
            guard let cartItemsArray = cartItems, success == true else {
                self.cartUpdateHandler?(false)
                completionHandler?(false)
                return
            }
            self.cartArray.removeAll()
            if cartItemsArray.count == 0 {
                self.cartUpdateHandler?(success)
            } else {
                for cartItem in cartItemsArray {
                    self.addCartItemToCart(cartItem) { success in
                        self.cartUpdateHandler?(success)
                        self.wishListUpdateHandler?(success)
                    }
                }
            }
            completionHandler?(true)
        }
    }
    
    func addProductToCart(_ product: Product, completionHandler: @escaping AddProductsRequestCompletionHandler) {
        if self.isStockSufficient(product: product) {
            self.networkManager?.addProductToCart(product.id) { [unowned self] responseCode in
                switch responseCode {
                case .itemAdded:
                    completionHandler(.itemAdded)
                    self.updateCartItems()
                case .authError, .none:
                    completionHandler(.networkError)
                case .outOfStock:
                    completionHandler(.stockNotSufficient)
                case .productNotFound:
                    completionHandler(.productNotFound)
                }
            }
        } else {
            completionHandler(.stockNotSufficient)
        }
    }
    
    func removeSingleItem(_ cartProduct: CartProduct, completionHandler: @escaping ProductsRequestCompletionHandler) {
        if let existingProductEnumeration = self.cartArray.enumerated().first(where: {$0.element.id == cartProduct.id}) {
            guard let firstCartId = existingProductEnumeration.element.cartItemIds.first else {
                completionHandler(false)
                return
            }
            self.networkManager?.removeProductFromCart(firstCartId) { [unowned self] resposeCode in
                var success = true
                switch resposeCode {
                case .itemRemoved:
                    existingProductEnumeration.element.quantity -= 1
                    existingProductEnumeration.element.cartItemIds.remove(at: 0)
                    if existingProductEnumeration.element.quantity == 0 {
                        self.cartArray.remove(at: existingProductEnumeration.offset)
                    }
                    self.cartUpdateHandler?(true)
                    self.wishListUpdateHandler?(true)
                default:
                    success = false
                }
                completionHandler(success)
            }
        }
    }
    
    func removeCartProduct(_ cartProduct: CartProduct, completionHandler: @escaping ProductsRequestCompletionHandler) {
        if let existingProductEnumeration = self.cartArray.enumerated().first(where: {$0.element.id == cartProduct.id}) {
            var success = true
            for id in existingProductEnumeration.element.cartItemIds {
                self.networkManager?.removeProductFromCart(id) { [unowned self] resposeCode in
                    switch resposeCode {
                    case .itemRemoved:
                        existingProductEnumeration.element.quantity -= 1
                        if existingProductEnumeration.element.quantity == 0 {
                            self.cartArray.remove(at: existingProductEnumeration.offset)
                        }
                        self.cartUpdateHandler?(true)
                        self.wishListUpdateHandler?(true)
                    default:
                        success = false
                    }
                }
            }
            completionHandler(success)
        } else {
            completionHandler(false)
        }
    }
    
    func addProductToWishList(_ product: Product, completionHandler: @escaping ProductsRequestCompletionHandler) {
        if let existingProductEnumeration = self.wishListArray.enumerated().first(where: {$0.element.id == product.id}) {
            self.wishListArray.remove(at: existingProductEnumeration.offset)
            completionHandler(false)
        } else {
            self.wishListArray.append(product)
            completionHandler(true)
        }
        self.wishListUpdateHandler?(true)
    }
    
    func moveProductFromWishlistToCart(_ product: Product, completionHandler: @escaping AddProductsRequestCompletionHandler) {
        if let _ = self.wishListArray.enumerated().first(where: {$0.element.id == product.id}) {
            if product.stock == 0 {
                completionHandler(.stockNotSufficient)
                return
            }
            self.addProductToCart(product) { result in
                completionHandler(result)
            }
        } else {
            completionHandler(.productNotFound)
        }
    }
    
    func isProductInWishlist(_ product: Product) -> Bool {
        return self.wishListArray.contains(where: {$0.id == product.id})
    }
    
    func isProductInCart(_ product: Product) -> Bool {
        return self.cartArray.contains(where: {$0.id == product.id})
    }
    
    func orderTotal() -> String {
        var total = 0.0
        for cartProduct in cartArray {
            if let price = Double(cartProduct.price) {
                total += price * Double(cartProduct.quantity)
            }
        }
        
        return String(format: "£%.2f", total)
    }
    
    func itemsInCart() -> Int {
        return cartArray.lazy.map { $0.quantity }.reduce(0, +)
    }
    
    func orderSavings() -> String {
        var total = 0.0
        for cartProduct in cartArray {
            if let price = Double(cartProduct.price), let oldPrice = cartProduct.oldPrice, let discountedPrice = Double(oldPrice) {
                total += (discountedPrice - price) * Double(cartProduct.quantity)
            }
        }
        
        return String(format: "£%.2f", total)
    }
    
    func archiveAndSaveWishList() {
        let encoder = JSONEncoder()
        if let data = try? encoder.encode(self.wishListArray) {
            UserDefaults.standard.set(data, forKey: self.archiveKey)
        }
    }
    
    func loadArchivedWishList() {
        let decoder = JSONDecoder()
        let data = UserDefaults.standard.data(forKey: self.archiveKey)
        
        if let wishListData = data, let wishListArray = try? decoder.decode([Product].self, from: wishListData) {
            self.wishListArray = wishListArray
        }
    }
    
    // MARK: Private Methods
    private func isStockSufficient(product: Product) -> Bool {
        if let existingCartProduct = self.cartArray.first(where: {$0.id == product.id}) {
            return existingCartProduct.quantity < product.stock
        }
        return true
    }
    
    private func findProductWithId(_ productId: Int) -> Product? {
        if let existingProduct = self.productsArray.first(where: {$0.id == productId}) {
            return existingProduct
        }
        return nil
    }
    
    private func addCartItemToCart(_ cartItem: CartItem, completionHandler: @escaping ProductsRequestCompletionHandler) {
        if let existingProduct = self.cartArray.first(where: {$0.id == cartItem.productId}) {
            existingProduct.quantity += 1
            existingProduct.cartItemIds.append(cartItem.id)
        } else {
            if let product = self.findProductWithId(cartItem.productId) {
                let cartProduct = CartProduct(product: product, cartItemId: cartItem.id)
                self.cartArray.append(cartProduct)
            } else {
                completionHandler(false)
                return
            }
        }
        completionHandler(true)
    }
    
    private func updateWishListStock() {
        for product in self.productsArray {
            if let wishListProduct = self.wishListArray.enumerated().first(where: {$0.element.id == product.id}) {
                self.wishListArray[wishListProduct.offset] = product
            }
        }
    }
}
