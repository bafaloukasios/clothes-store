//
//  CartItem.swift
//  Clothes Store
//
//  Created by George Bafaloukas on 16/02/2020.
//  Copyright © 2020 George Bafaloukas. All rights reserved.
//

import UIKit

public struct CartItem: Codable {
    var id: Int
    var productId: Int
}
