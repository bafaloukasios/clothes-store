//
//  CartProduct.swift
//  Clothes Store
//
//  Created by George Bafaloukas on 15/02/2020.
//  Copyright © 2020 George Bafaloukas. All rights reserved.
//

import UIKit

class CartProduct: Product {
    var quantity: Int = 1
    var cartItemIds: [Int] = []
    init(product: Product, cartItemId: Int) {
        super.init(id: product.id, name: product.name, category: product.category, price: product.price, oldPrice: product.oldPrice, stock: product.stock)
        self.cartItemIds.append(cartItemId)
    }
    
    required init(from decoder: Decoder) throws {
        try super.init(from: decoder)
    }
}
