//
//  Enums.swift
//  Clothes Store
//
//  Created by George Bafaloukas on 16/02/2020.
//  Copyright © 2020 George Bafaloukas. All rights reserved.
//

import UIKit

public enum AddProductResult: String {
    case itemAdded = "Item added"
    case stockNotSufficient = "Stock not sufficient"
    case productNotFound = "Product not found"
    case networkError = "Network error"
}

public enum AddToCartReponseCode: Int {
    case itemAdded = 201
    case authError = 401
    case outOfStock = 403
    case productNotFound = 404
}

public enum RemoveCartReponseCode: Int {
    case itemRemoved = 204
    case authError = 401
    case productNotFound = 404
}
