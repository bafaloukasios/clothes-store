//
//  Product.swift
//  Clothes Store
//
//  Created by George Bafaloukas on 15/02/2020.
//  Copyright © 2020 George Bafaloukas. All rights reserved.
//

import UIKit

public class Product: Codable {
    var id: Int
    var name: String
    var category: String
    var price: String
    var oldPrice: String?
    var stock: Int
    
    init(id: Int, name: String, category: String, price: String, oldPrice: String?, stock: Int) {
        self.id = id
        self.name = name
        self.category = category
        self.price = price
        self.oldPrice = oldPrice
        self.stock = stock
    }
}
